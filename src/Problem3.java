import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num1;
        int num2;
        System.out.print("Please input first number:");
        num1 = sc.nextInt();
        System.out.print("Please input second number:");
        num2 = sc.nextInt();
        if (num1 > num2) {
            System.out.print("Error");
        }
        while (num1 <= num2) {
            System.out.print(num1 + " ");
            num1++;
        }
        sc.close();
    }
}
