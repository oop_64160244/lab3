import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num ;
        int sum = 0;
        int count = 0;
        while(true){
            System.out.print("Please input number:");
            num = sc.nextInt();
            if(num==0) {
                break ;
            }

            sum = sum + num;
            count++;
            System.out.println("Sum: "+ sum +" Avg: "+((double)sum)/count);
        }
        System.out.println("Bye");
        sc.close();
    }
}
